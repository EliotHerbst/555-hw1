package edu.upenn.cis.cis455.m1.server;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.logging.log4j.Level;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.handling.FileRequestHandler;
import edu.upenn.cis.cis455.m1.interfaces.HttpResponse;

public class TestFileRequestHandler {
	private static final String TESTDIRECTORY= File.separator + "vagrant" + File.separator + "TestFolder";
	private static final String TESTFILE = File.separator + "vagrant" + File.separator + "TestFolder" + File.separator + "test.txt";
	private static final String FILEPATH = File.separator + "test.txt";
	private static final String INDEXHTML = File.separator + "index.html";
	private static final String MESSAGE = "Test Message";
	private static final String HTMLMESSAGE = "<p> Text <p>";
	
    @Before
    public void setUp() throws IOException {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
        File testDirectory = new File(TestFileRequestHandler.TESTDIRECTORY);
        testDirectory.mkdir();
        File testFile = new File(TestFileRequestHandler.TESTFILE);
        File indexFile = new File(TestFileRequestHandler.TESTDIRECTORY + TestFileRequestHandler.INDEXHTML);
        try {
			testFile.createNewFile();
			FileWriter writer = new FileWriter(TestFileRequestHandler.TESTFILE);
			writer.write(TestFileRequestHandler.MESSAGE);
			writer.close();
			FileWriter secondWriter = new FileWriter(TestFileRequestHandler.TESTDIRECTORY + TestFileRequestHandler.INDEXHTML);
			secondWriter.write(TestFileRequestHandler.HTMLMESSAGE);
			secondWriter.close();
        } catch (IOException e) {
			throw new IOException();
		} 
    }
    
    @Test
    public void testGetFile() {
    	File testFile = FileRequestHandler.getFile(TestFileRequestHandler.FILEPATH, TestFileRequestHandler.TESTDIRECTORY);
    	
    	assert(testFile.exists());
    	assert(testFile.isFile());
    }
    
    @Test (expected = HaltException.class)
    public void testGetFileNotExist() {
    	FileRequestHandler.getFile("cat.txt", TestFileRequestHandler.TESTDIRECTORY);
    }
    
    @Test (expected = HaltException.class)
    public void testForbidden() {
    	FileRequestHandler.getFile(".." + File.separator + "555-hw1" + File.separator + "www" + File.separator + "index.html", TestFileRequestHandler.TESTDIRECTORY);
    }
    
    @Test
    public void testGetsIndexHtml() {
    	File testFile = FileRequestHandler.getFile(File.separator, TestFileRequestHandler.TESTDIRECTORY);
    	
    	assert(testFile.exists());
    	assert(testFile.isFile());
    }
    
    @Test
    public void testFileToResponse() {
    	HttpResponse rsp = FileRequestHandler.fileToResponse(FileRequestHandler.getFile(TestFileRequestHandler.FILEPATH, TestFileRequestHandler.TESTDIRECTORY));
    	
    	assertEquals(rsp.status(), 200);
    	
    	byte[] expected = TestFileRequestHandler.MESSAGE.getBytes();
    	
    	assertArrayEquals(rsp.bodyRaw(), expected);
    	
    	assertEquals(rsp.type(), "text/plain");
  
    }
    
    @After
    public void tearDown() {
    	File testFile = new File(TestFileRequestHandler.TESTFILE);
    	testFile.delete();
    	File indexFile = new File(TestFileRequestHandler.TESTDIRECTORY + TestFileRequestHandler.INDEXHTML);
    	indexFile.delete();
    	File testDirectory = new File(TestFileRequestHandler.TESTDIRECTORY);
    	testDirectory.delete();
    }
}
