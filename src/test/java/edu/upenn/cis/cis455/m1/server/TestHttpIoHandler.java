package edu.upenn.cis.cis455.m1.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.Socket;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.cis455.TestHelper;
import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
import edu.upenn.cis.cis455.m1.handling.HttpParser;
import edu.upenn.cis.cis455.m1.interfaces.HttpRequest;

public class TestHttpIoHandler {
	
		private static final String MESSAGE = "This is a sample\nMessage for testing";

		@Before
		public void setUp() {
			org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
		}
		
		@Test
		public void testReadFromSocket() throws IOException{
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	        Socket s = TestHelper.getMockSocket(
	            TestHttpIoHandler.MESSAGE, 
	            byteArrayOutputStream);
	        
	        BufferedReader result = HttpIoHandler.readFromSocket(s);
	        assertEquals(result.lines().collect(Collectors.joining("\n")), TestHttpIoHandler.MESSAGE);
		}
		
		@Test
		public void testReaderFromSocketOneLine() throws IOException {
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	        Socket s = TestHelper.getMockSocket(
	            "Cat", 
	            byteArrayOutputStream);
	        BufferedReader result = HttpIoHandler.readFromSocket(s);
	        assertEquals(result.lines().collect(Collectors.joining("\n")), "Cat");
		}
		
		@Test
		public void testReaderFromSocketEmpty() throws IOException {
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	        Socket s = TestHelper.getMockSocket(
	            "", 
	            byteArrayOutputStream);
	        BufferedReader result = HttpIoHandler.readFromSocket(s);
	        assertEquals(result.lines().collect(Collectors.joining("\n")), "");
		}
		
		String headRequest =
	            "HEAD /index.html HTTP/1.1\r\n" +
	            "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	            "Host: www.cis.upenn.edu\r\n" +
	            "Accept-Language: en-us\r\n" +
	            "Accept-Encoding: gzip, deflate\r\n" +
	            "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
	            "Connection: Keep-Alive\r\n\r\n";
		
		@Test
		public void testHead() throws IOException {
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	        Socket s = TestHelper.getMockSocket(
	            headRequest, 
	            byteArrayOutputStream);
	        BufferedReader br1 = HttpIoHandler.readFromSocket(s);
    		HttpRequest req = HttpParser.decodeRequest(br1);
	        HttpIoHandler.sendResponse(s, req, null);
	        String result = byteArrayOutputStream.toString("UTF-8");
	        Reader read = new StringReader(result);
	        BufferedReader br = new BufferedReader(read);
	        br.readLine();
	        assertTrue(result.startsWith("HTTP/1.1 200 OK"));
	        String next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        assertTrue(br.readLine().isBlank());
	        assertNull(br.readLine());
		}
		
		String modifiedSinceRequestF1 =
	            "GET / HTTP/1.1\r\n" +
	            "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	            "Host: www.cis.upenn.edu\r\n" +
	            "Accept-Language: en-us\r\n" +
	            "If-Modified-Since: Fri, 31 Dec 1999 23:59:59 GMT\r\n" +
	            "Accept-Encoding: gzip, deflate\r\n" +
	            "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
	            "Connection: Keep-Alive\r\n\r\n";
		
		@Test
		public void testModifiedSinceF1() throws IOException {
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	        Socket s = TestHelper.getMockSocket(
	            modifiedSinceRequestF1, 
	            byteArrayOutputStream);
	        BufferedReader br1 = HttpIoHandler.readFromSocket(s);
    		HttpRequest req = HttpParser.decodeRequest(br1);
	        HttpIoHandler.sendResponse(s, req, null);
	        String result = byteArrayOutputStream.toString("UTF-8");
	        Reader read = new StringReader(result);
	        BufferedReader br = new BufferedReader(read);
	        br.readLine();
	        assertTrue(result.startsWith("HTTP/1.1 200 OK"));
	        String next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        assertTrue(br.readLine().isBlank());
	        next = br.readLine();
	        assertTrue(next != null && !next.isBlank());
		}
		
		String modifiedSinceRequestF2 =
	            "GET / HTTP/1.1\r\n" +
	            "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	            "Host: www.cis.upenn.edu\r\n" +
	            "Accept-Language: en-us\r\n" +
	            "If-Modified-Since: Saturday, 01-Jan-00 23:59:59 GMT\r\n" +
	            "Accept-Encoding: gzip, deflate\r\n" +
	            "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
	            "Connection: Keep-Alive\r\n\r\n";
		
		@Test
		public void testModifiedSinceF2() throws IOException {
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	        Socket s = TestHelper.getMockSocket(
	            modifiedSinceRequestF2, 
	            byteArrayOutputStream);
	        BufferedReader br1 = HttpIoHandler.readFromSocket(s);
    		HttpRequest req = HttpParser.decodeRequest(br1);
	        HttpIoHandler.sendResponse(s, req, null);
	        String result = byteArrayOutputStream.toString("UTF-8");
	        Reader read = new StringReader(result);
	        BufferedReader br = new BufferedReader(read);
	        br.readLine();
	        assertTrue(result.startsWith("HTTP/1.1 200 OK"));
	        String next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        assertTrue(br.readLine().isBlank());
	        next = br.readLine();
	        assertTrue(next != null && !next.isBlank());
		}
		
		String modifiedSinceRequestF3 =
	            "GET / HTTP/1.1\r\n" +
	            "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	            "Host: www.cis.upenn.edu\r\n" +
	            "Accept-Language: en-us\r\n" +
	            "If-Modified-Since: Fri Dec 31 23:59:59 1999\r\n" + 
	            "Accept-Encoding: gzip, deflate\r\n" +
	            "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
	            "Connection: Keep-Alive\r\n\r\n";
		
		@Test
		public void testModifiedSinceF3() throws IOException {
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	        Socket s = TestHelper.getMockSocket(
	            modifiedSinceRequestF3, 
	            byteArrayOutputStream);
	        BufferedReader br1 = HttpIoHandler.readFromSocket(s);
    		HttpRequest req = HttpParser.decodeRequest(br1);
	        HttpIoHandler.sendResponse(s, req, null);
	        String result = byteArrayOutputStream.toString("UTF-8");
	        Reader read = new StringReader(result);
	        BufferedReader br = new BufferedReader(read);
	        br.readLine();
	        assertTrue(result.startsWith("HTTP/1.1 200 OK"));
	        String next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        next = br.readLine();
	        assertTrue(next.startsWith("Last-Modified:") || next.startsWith("Server:") || next.startsWith("Date:") || next.startsWith("Connection:") || next.startsWith("content-length") || next.startsWith("content-type"));
	        assertTrue(br.readLine().isBlank());
	        next = br.readLine();
	        assertTrue(next != null && !next.isBlank());
		}
		
		String modifiedSinceRequestInvalidDate =
	            "GET / HTTP/1.1\r\n" +
	            "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	            "Host: www.cis.upenn.edu\r\n" +
	            "Accept-Language: en-us\r\n" +
	            "If-Modified-Since: FriDec31 23:59:59 1999\r\n" + 
	            "Accept-Encoding: gzip, deflate\r\n" +
	            "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
	            "Connection: Keep-Alive\r\n\r\n";
		
		@Test
		public void testModifiedSinceRequestInvalidDate() throws IOException {
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	        Socket s = TestHelper.getMockSocket(
	            modifiedSinceRequestInvalidDate, 
	            byteArrayOutputStream);
	        BufferedReader br1 = HttpIoHandler.readFromSocket(s);
    		HttpRequest req = HttpParser.decodeRequest(br1);
	        HttpIoHandler.sendResponse(s, req, null);
	        String result = byteArrayOutputStream.toString("UTF-8");
	        Reader read = new StringReader(result);
	        assertTrue(result.startsWith("HTTP/1.1 400"));

		}
		
		String modifiedSinceRequestFuture=
	            "GET / HTTP/1.1\r\n" +
	            "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	            "Host: www.cis.upenn.edu\r\n" +
	            "Accept-Language: en-us\r\n" +
	            "If-Modified-Since: Mon Dec 31 23:59:59 2035\r\n" + 
	            "Accept-Encoding: gzip, deflate\r\n" +
	            "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
	            "Connection: Keep-Alive\r\n\r\n";
		
		@Test
		public void testModifiedSinceRequestNotModified() throws IOException {
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	        Socket s = TestHelper.getMockSocket(
	            modifiedSinceRequestFuture, 
	            byteArrayOutputStream);
	        BufferedReader br1 = HttpIoHandler.readFromSocket(s);
    		HttpRequest req = HttpParser.decodeRequest(br1);
	        HttpIoHandler.sendResponse(s, req, null);
	        String result = byteArrayOutputStream.toString("UTF-8");
	        Reader read = new StringReader(result);
	        assertTrue(result.startsWith("HTTP/1.1 304"));

		}
		
		String unmodifiedSinceRequestFuture=
	            "GET / HTTP/1.1\r\n" +
	            "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	            "Host: www.cis.upenn.edu\r\n" +
	            "Accept-Language: en-us\r\n" +
	            "If-Unmodified-Since: Mon Dec 31 23:59:59 2035\r\n" + 
	            "Accept-Encoding: gzip, deflate\r\n" +
	            "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
	            "Connection: Keep-Alive\r\n\r\n";
		
		@Test
		public void testUnModifiedSinceRequestNotModified() throws IOException {
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	        Socket s = TestHelper.getMockSocket(
	            unmodifiedSinceRequestFuture, 
	            byteArrayOutputStream);
	        BufferedReader br1 = HttpIoHandler.readFromSocket(s);
    		HttpRequest req = HttpParser.decodeRequest(br1);
	        HttpIoHandler.sendResponse(s, req, null);
	        String result = byteArrayOutputStream.toString("UTF-8");
	        Reader read = new StringReader(result);
	        assertTrue(result.startsWith("HTTP/1.1 200"));

		}
		
		String unmodifiedSinceRequestPast=
	            "GET / HTTP/1.1\r\n" +
	            "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	            "Host: www.cis.upenn.edu\r\n" +
	            "Accept-Language: en-us\r\n" +
	            "If-Unmodified-Since: Sat Dec 31 23:59:59 1960\r\n" + 
	            "Accept-Encoding: gzip, deflate\r\n" +
	            "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
	            "Connection: Keep-Alive\r\n\r\n";
		
		@Test
		public void testUnModifiedSinceRequestModified() throws IOException {
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	        Socket s = TestHelper.getMockSocket(
	            unmodifiedSinceRequestPast, 
	            byteArrayOutputStream);
	        BufferedReader br1 = HttpIoHandler.readFromSocket(s);
    		HttpRequest req = HttpParser.decodeRequest(br1);
	        HttpIoHandler.sendResponse(s, req, null);
	        String result = byteArrayOutputStream.toString("UTF-8");
	        Reader read = new StringReader(result);
	        assertTrue(result.startsWith("HTTP/1.1 412"));

		}
		
		
		
		@After
		public void tearDown() { }
}
