package edu.upenn.cis.cis455.m1.server;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.Level;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.handling.HttpParser;
import edu.upenn.cis.cis455.m1.interfaces.HttpRequest;
import edu.upenn.cis.cis455.m1.interfaces.Request;

public class TestHttpParser {
	@Before
	public void setUp() {
		org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
	}
	
	String sampleGetRequest = 
	        "GET /a/uri/here HTTP/1.1\r\n" +
	        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	        "Host: www.cis.upenn.edu\r\n" +
	        "Accept-Language: en-us\r\n" +
	        "Accept-Encoding: gzip, deflate\r\n" +
	        "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
	        "Connection: Keep-Alive\r\n\r\n";
	
	@Test
	public void testHttpParser() {
		Reader input = new StringReader(sampleGetRequest);
		BufferedReader br = new BufferedReader(input);
		HttpRequest req = HttpParser.decodeRequest(br);
		assertEquals(req.protocol(), "HTTP/1.1");
		assertEquals(req.uri(), "/a/uri/here");
		assertEquals(req.url(), "/a/uri/here");
		assertEquals(req.pathInfo(), "/a/uri/here");
		assertEquals(req.requestMethod(), "GET");
		Set<String> headers = new HashSet<String>();
		headers.add("user-agent");
		headers.add("connection");
		headers.add("host");
		assertEquals(req.headers(), headers);
		assertEquals(req.headers("user-agent"), "Mozilla/4.0 (compatible; MSIE5.01; Windows NT)");
		assertEquals(req.headers("host"), "www.cis.upenn.edu");
		assertEquals(req.headers("connection"), "keep-alive");
		
	}
	
	@Test (expected = HaltException.class)
	public void testHttpParserEmpty() {
		Reader input = new StringReader("");
		BufferedReader br = new BufferedReader(input);
		HttpParser.decodeRequest(br);
	}
	
	String sampleGetRequestMalformed = 
	        "/a/uri/here HTTP/1.1\r\n" +
	        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	        "Host: www.cis.upenn.edu\r\n" +
	        "Accept-Language: en-us\r\n" +
	        "Accept-Encoding: gzip, deflate\r\n" +
	        "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
	        "Connection: Keep-Alive\r\n\r\n";
	
	@Test (expected = HaltException.class)
	public void testHttpParserNoMethod() {
		Reader input = new StringReader(sampleGetRequestMalformed);
		BufferedReader br = new BufferedReader(input);
		HttpParser.decodeRequest(br);
	}
	
	String sampleGetRequestMalformed2 = 
	        "GET HTTP/1.1\r\n" +
	        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	        "Host: www.cis.upenn.edu\r\n" +
	        "Accept-Language: en-us\r\n" +
	        "Accept-Encoding: gzip, deflate\r\n" +
	        "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
	        "Connection: Keep-Alive\r\n\r\n";
	
	@Test (expected = HaltException.class)
	public void testHttpParserNoUri() {
		Reader input = new StringReader(sampleGetRequestMalformed2);
		BufferedReader br = new BufferedReader(input);
		HttpParser.decodeRequest(br);
	}
	
	String sampleGetRequestMalformed3 = 
	        "GET /a/uri/here\r\n" +
	        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	        "Host: www.cis.upenn.edu\r\n" +
	        "Accept-Language: en-us\r\n" +
	        "Accept-Encoding: gzip, deflate\r\n" +
	        "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
	        "Connection: Keep-Alive\r\n\r\n";
	
	@Test (expected = HaltException.class)
	public void testHttpParserNoProtocol() {
		Reader input = new StringReader(sampleGetRequestMalformed3);
		BufferedReader br = new BufferedReader(input);
		HttpParser.decodeRequest(br);
	}
	
	String sampleGetRequestMalformed4 = 
	        "GET /a/uri/here HTTP/1.1\r\n" +
	        "User-Agent Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	        "Host www.cis.upenn.edu\r\n" +
	        "Accept-Language en-us\r\n" +
	        "Accept-Encoding gzip, deflate\r\n" +
	        "Cookie name1=value1; name2=value2; name3=value3\r\n" +
	        "Connection Keep-Alive\r\n\r\n";
	
	@Test
	public void testHttpParserNoColons() {
		Reader input = new StringReader(sampleGetRequestMalformed4);
		BufferedReader br = new BufferedReader(input);
		HttpRequest req = HttpParser.decodeRequest(br);
		assertEquals(req.protocol(), "HTTP/1.1");
		assertEquals(req.uri(), "/a/uri/here");
		assertEquals(req.requestMethod(), "GET");
		assertEquals(req.headers().size(), 0);
	}
	
	
	String sampleGetRequestFull = 
	        "GET http://localhost:45555/cat/dog/?success=True HTTP/1.1\r\n" +
	        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
	        "Host: www.cis.upenn.edu\r\n" +
	        "Accept-Language: en-us\r\n" +
	        "Accept-Encoding: gzip, deflate\r\n" +
	        "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
	        "Connection: Keep-Alive\r\n\r\n";
	
	@Test
	public void testFullUrl() {
		Reader input = new StringReader(sampleGetRequestFull);
		BufferedReader br = new BufferedReader(input);
		HttpRequest req = HttpParser.decodeRequest(br);
		Set<String> headers = new HashSet<String>();
		headers.add("user-agent");
		headers.add("host");
		headers.add("connection");
		assertEquals(req.headers(), headers);
		assertEquals(req.headers("user-agent"), "Mozilla/4.0 (compatible; MSIE5.01; Windows NT)");
		assertEquals(req.headers("host"), "www.cis.upenn.edu");
		assertEquals(req.persistentConnection(), true);
		assertEquals(req.host(), "www.cis.upenn.edu");
		assertEquals(req.url(), "http://localhost:45555/cat/dog/?success=True");
		assertEquals(req.uri(), "http://localhost:45555/cat/dog/");
		assertEquals(req.pathInfo(), "/cat/dog/");
		assertEquals(req.protocol(), "HTTP/1.1");
		assertEquals(req.requestMethod(), "GET");
	}
	
	@After
	public void tearDown() {
		
	}
}
