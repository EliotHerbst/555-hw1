package edu.upenn.cis.cis455;

import static edu.upenn.cis.cis455.SparkController.*;

import java.io.File;

import org.apache.logging.log4j.Level;

/**
 * Initialization / skeleton class.
 * Note that this should set up a basic web server for Milestone 1.
 * For Milestone 2 you can use this to set up a basic server.
 * 
 * CAUTION - ASSUME WE WILL REPLACE THIS WHEN WE TEST MILESTONE 2,
 * SO ALL OF YOUR METHODS SHOULD USE THE STANDARD INTERFACES.
 * 
 * @author zives
 *
 */
public class WebServer {
    public static void main(String[] args) {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);

        // TODO: make sure you parse *BOTH* command line arguments properly
        // Check if command line arguments were given
        if (args.length == 2) {
        	// Try to parse and set port
        	try {
        		int port = Integer.parseInt(args[0]);
        		port(port);
        	} catch (NumberFormatException e) {
        		throw new IllegalArgumentException("Port unable to be parsed, port must be numeric, ex. 45555");
        	}
        	// Try to parse and set directory
        	String directory = args[1];
        	File dir = new File(directory);
        	if (!dir.exists()) {
        		throw new IllegalArgumentException("Directory does not Exist");
        	}
        	if(!dir.isDirectory()) {
        		throw new IllegalArgumentException("Given path is not a directory");
        	}
        	staticFileLocation(directory);
        }
        // Check if argument is port or directory if just one argument is given
        if (args.length == 1) {
        	try {
        		int port = Integer.parseInt(args[0]);
        		port(port);
        	} catch (NumberFormatException e) {
        		String directory = args[0];
        		File dir = new File(directory);
        		if (!dir.exists()) {
        			throw new IllegalArgumentException("Argument is not a port or valid directory");
        		}
        		if (!dir.isDirectory()) {
        			throw new IllegalArgumentException("Argument is not a port or valid directory");
        		}
        		staticFileLocation(directory);
        	}
        }
        // Throw Exception if more than 2 arguments are given
        if (args.length > 2) {
        	throw new IllegalArgumentException(args.length + " arguments recieved, 2 expected");
        }
        
        // Set Initial IP Address
        ipAddress("0.0.0.0");
        // Set thread pool thread worker count
        threadPool(16);
        // All user routes should go below here...

        // ... and above here. Leave this comment for the Spark comparator tool

        System.out.println("Waiting to handle requests!");
        awaitInitialization();
    }
}
