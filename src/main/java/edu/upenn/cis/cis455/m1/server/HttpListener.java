package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Stub for your HTTP server, which listens on a ServerSocket and handles
 * requests
 */
public class HttpListener implements Runnable {
	final static Logger logger = LogManager.getLogger(HttpListener.class);
	
	private HttpTaskQueue httpTaskQueue;
	private List<HttpWorker> httpWorkerPool;
	private int poolSize;
	private int port;
	private String ipAddress;
	private String directory;
	private ServerSocket serverSocket;
	private volatile boolean stop = false; // Flag to stop 
	
	/**
	 * Sets Pool Size
	 * @param poolSize the new poolSize
	 */
	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}
	
	/**
	 * Sets port
	 * @param port the new port
	 */
	public void setPort(int port) {
		this.port = port;
	}
	
	/**
	 * Sets IP Address
	 * @param ipAddress the new ip address
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	/**
	 * Sets Directory
	 * @param directory the new directory
	 */
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	
	/**
	 * Initializes the worker pool
	 */
	private void initializeWorkerPool() {
		//logger.info("Starting Worker Pool");
		this.httpWorkerPool = new ArrayList<HttpWorker>();
		for(int i = 0; i < this.poolSize; i++) {
			this.httpWorkerPool.add(new HttpWorker(this.httpTaskQueue, i));
			Thread thread = new Thread(httpWorkerPool.get(i));
			thread.setName("Worker-" + i);
			thread.start();
		}
		//logger.info("Worker Pool Started");
		
	}
	
	/**
	 * Sets up this {@code HttpListener}
	 */
	public void setup() {
		this.httpTaskQueue = new HttpTaskQueue(128);
		//logger.info("HttpTaskQueue Created");
		this.initializeWorkerPool();
	}
	
	/**
	 * Gracefully shuts down this {@code HttpListener}
	 */
	public void tearDown() {
		this.stop = true; // Stop Listener main loop
		try {
			serverSocket.close(); // Stop Listener main loop, causes accept to error out
		} catch (IOException e) {
			
		}
		// Stop every worker
		for (HttpWorker worker : this.httpWorkerPool) {
			worker.stop();
		}
		
		// Stop the Task Queue from blocking
		httpTaskQueue.stop();
	}
	
	public List<String> workerInfo() {
		List<String> workersInfo = new ArrayList<String>();
		for(HttpWorker worker : this.httpWorkerPool) {
			workersInfo.add(worker.getInfo());
		}
		return workersInfo;
	}

	/**
	 * Creates {@code ServerSocket}, listens and adds new connections to the {@code HttpTaskQueue} as
	 * {@code HttpTasks}
	 */
    @Override
    public void run() {
    	// Create ServerSocket to recieve messages
    	serverSocket = null;
		try {
			serverSocket = new ServerSocket(this.port, 50,InetAddress.getByName(this.ipAddress));
		} catch (UnknownHostException e1) {
			logger.catching(e1);
		} catch (IOException e) {
			logger.catching(e);
		}
		// Main Loop
        while(!stop) {
        	try {
        		// Accept a Connection
				Socket connection = serverSocket.accept();
				
				// Add it to the Queue
				while(this.httpTaskQueue.addTask(new HttpTask(connection)) == false) { }
			} catch (SocketException e) {
				// Shutting Down
				break;
			} catch (IOException e) {
				logger.catching(e);
			} catch (InterruptedException e) {
				logger.catching(e);
			} 
        }
        try {
        	serverSocket.close();
        } catch(IOException e) {
        	logger.catching(e);
        }
        
    }
}
