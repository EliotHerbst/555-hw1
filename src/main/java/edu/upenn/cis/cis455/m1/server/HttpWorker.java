package edu.upenn.cis.cis455.m1.server;

import java.io.BufferedReader;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
import edu.upenn.cis.cis455.m1.handling.HttpParser;
import edu.upenn.cis.cis455.m1.interfaces.HttpRequest;

/**
 * Stub class for a thread worker that handles Web requests
 */
public class HttpWorker implements Runnable {

	final static Logger logger = LogManager.getLogger(HttpWorker.class);
	
	private HttpTaskQueue httpTaskQueue;
	
	private HttpRequest currentRequest; 
	
	private int threadNumber;

	private volatile boolean stop; // Flag for stopping the worker
	
	/**
	 * Creates a {@code HttpWorker} which works off of the given
	 * {@code HttpTaskQueue}
	 * @param httpTaskQueue The task queue to work of off
	 */
	public HttpWorker(HttpTaskQueue httpTaskQueue, int threadNumber) {
		super();
		this.httpTaskQueue = httpTaskQueue;
		this.stop = false;
		this.currentRequest = null;
		this.threadNumber = threadNumber;
	}
	
	/**
	 * Stops this {@code HttpWorker}
	 */
	public void stop() {
		if (!this.stop) {
			this.stop = true;
		}
	}
	
	public String getInfo() {
		String threadName = "Worker-" + this.threadNumber;
		String url = currentRequest == null ? "waiting" : currentRequest.url();
		return "ThreadName: " + threadName + " CurrentStatus: " + url;
	}

	/**
	 * Starts this {@code HttpWorker}. Draws Tasks from the queue and performs them.
	 */
    @Override
    public void run() {
    	//logger.info("Starting Worker-" + this.threadNumber);
        while(!stop) {
        	currentRequest = null;
        	try {
        		HttpTask nextTask = this.httpTaskQueue.nextTask();
        		if(nextTask != null) {
                	// Get Filled Request from BufferedReader
                	try {
                		Socket socket = nextTask.getSocket();
                		// Get BufferedReader from socket
                    	BufferedReader br = HttpIoHandler.readFromSocket(socket);
                		HttpRequest req = HttpParser.decodeRequest(br);
                		this.currentRequest = req;
                		//logger.info("Worker-" + this.threadNumber + " working on Request: " + this.currentRequest.url());
                		//Add ip to req
                    	req.ip(((InetSocketAddress) socket.getRemoteSocketAddress()).getAddress().toString());
                    	// Add port to req
                    	req.port(socket.getPort());
            			HttpIoHandler.sendResponse(socket, req, null);
            			
                	} catch (HaltException e) {
                		HttpIoHandler.sendException(nextTask.getSocket(), null, e);
                	} catch (Exception e) {
                		logger.catching(e);
                	}
        		}
        	} catch(InterruptedException e) {
        		logger.catching(e);
        	}
        }
        //logger.info("Stopping Worker");
    }
}
