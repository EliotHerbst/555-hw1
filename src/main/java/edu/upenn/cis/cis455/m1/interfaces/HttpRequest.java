package edu.upenn.cis.cis455.m1.interfaces;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class HttpRequest extends Request{
	
	private String method; 
	
	private String host;
	
	private String userAgent;
	
	private int port;
	
	private String accept;
	
	private String pathInfo;
	
	private String url;
	
	private String uri;
	
	private String protocol;
	
	private String contentType;
	
	private String ip;
	
	private String body;
	
	private String ifModifiedSince;
	
	private String ifUnmodifiedSince;
	
	private int contentLength;
	
	private String headers;
	
	private Map<String, String> headerMap;
	
	private Set<String> headerSet;
	
	/**
	 * Create new Http Request Object
	 */
	public HttpRequest() {
		this.headerMap = new HashMap<String, String>();
		this.headerSet = new HashSet<String>();
	}
	
	/**
	 * Returns the requestMethod
	 */
	@Override
	public String requestMethod() {
		return this.method;
	}
	
	/**
	 * sets the method
	 * @param method method to set
	 */
	public void method(String method) {
		this.method = method;
	}
	
	public String accept() {
		return this.accept;
	}
	
	public void accept(String accept) {
		if (accept == null) {
			this.headerSet.remove("accept");
			this.headerMap.remove("accept");
			this.accept = null;
			return;
		}
		this.headerSet.add("accept");
		this.headerMap.put("accept", accept);
		this.accept = accept;
	}

	/**
	 * Returns the host value
	 */
	@Override
	public String host() {
		return this.host;
	}
	
	/**
	 * Sets the host value
	 * @param host
	 */
	public void host(String host) {
		if (host == null) {
			this.headerSet.remove("host");
			this.headerMap.remove("host");
			this.host = null;
			return;
		}
		this.host = host;
		this.headerSet.add("host");
		this.headerMap.put("host", host);
	}

	public String ifModifiedSince() {
		return this.ifModifiedSince;
	}
	

	public void ifModifiedSince(String ifModifiedSince) {
		if (ifModifiedSince == null) {
			this.headerSet.remove("if-modified-since");
			this.headerMap.remove("if-modified-since");
			this.ifModifiedSince = null;
			return;
		}
		this.ifModifiedSince = ifModifiedSince;
		this.headerSet.add("if-modified-since");
		this.headerMap.put("if-modified-since", ifModifiedSince);
		
	}
	
	public String ifUnmodifiedSince() {
		return this.ifUnmodifiedSince;
	}
	

	public void ifUnmodifiedSince(String ifUnmodifiedSince) {
		if (ifUnmodifiedSince == null) {
			this.headerSet.remove("if-unmodified-since");
			this.headerMap.remove("if-unmodified-since");
			this.ifUnmodifiedSince = null;
			return;
		}
		this.ifUnmodifiedSince = ifUnmodifiedSince;
		this.headerSet.add("if-unmodified-since");
		this.headerMap.put("if-unmodified-since", ifUnmodifiedSince);
		
	}

	/**
	 * Get the userAgent
	 */
	@Override
	public String userAgent() {
		return this.userAgent;
	}
	
	/**
	 * Sets the user Agent
	 * @param userAgent String to set userAgent to
	 */
	public void userAgent(String userAgent) {
		if (userAgent == null) {
			this.headerSet.remove("user-agent");
			this.headerMap.remove("user-agent");
			this.userAgent = null;
			return;
		}
		this.userAgent = userAgent;
		this.headerSet.add("user-agent");
		this.headerMap.put("user-agent", userAgent);
		
	}

	@Override
	public int port() {
		return this.port;
	}
	
	/**
	 * Sets the port
	 * @param port New port value
	 */
	public void port(int port) {
		this.port = port;
	}

	@Override
	public String pathInfo() {
		return this.pathInfo;
	}
	
	/**
	 * Sets the path info
	 * @param pathInfo new Path Info
	 */
	public void pathInfo(String pathInfo) {
		this.pathInfo = pathInfo;
	}

	@Override
	public String url() {
		return this.url;
	}
	
	/**
	 * Sets the url
	 * @param url new Url
	 */
	public void url(String url) {
		this.url = url;
	}

	@Override
	public String uri() {
		return this.uri;
	}
	
	/**
	 * Sets the uri
	 * @param uri new Uri
	 */
	public void uri(String uri) {
		this.uri = uri;
	}

	@Override
	public String protocol() {
		return this.protocol;
	}
	
	/**
	 * Sets the protocol
	 * @param protocol new protocol
	 */
	public void protocol(String protocol) {
		this.protocol = protocol;
	}

	@Override
	public String contentType() {
		return this.contentType;
	}
	
	/**
	 * Sets content Type
	 * @param contentType
	 */
	public void contentType(String contentType) {
		if (contentType == null) {
			this.headerSet.remove("content-type");
			this.headerMap.remove("content-type");
			this.contentType = null;
			return;
		}
		this.headerSet.add("content-type");
		this.headerMap.put("content-type", contentType);
		this.contentType = contentType;
	}

	@Override
	public String ip() {
		return this.ip;
	}
	
	/**
	 * Sets IP	
	 * @param ip the new ip
	 */
	public void ip(String ip) {
		this.ip = ip;
	}

	@Override
	public String body() {
		return this.body;
	}
	
	/**
	 * Sets body
	 * @param body new body
	 */
	public void body(String body) {
		this.body = body;
		if (body == null) {
			this.headerSet.remove("content-length");
			this.headerMap.remove("content-length");
		} else {
			this.headerSet.add("content-length");
			this.headerMap.put("content-length", "" + body.getBytes().length);
		}
	}
	
	@Override
	public void persistentConnection(boolean b) {
		this.headerSet.add("connection");
		if (b) {
			this.headerMap.put("connection", "keep-alive");
		} else {
			this.headerMap.put("connection", "close");
		}
		this.persistent = b;
	}

	@Override
	public int contentLength() {
		return this.contentLength;
	}

	@Override
	public String headers(String name) {
		if (this.headerSet.contains(name)) {
			return this.headerMap.get(name);
		}
		return null;
	}

	@Override
	public Set<String> headers() {
		return this.headerSet;
	}

}
