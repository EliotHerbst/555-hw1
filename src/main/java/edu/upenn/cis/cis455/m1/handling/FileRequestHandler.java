package edu.upenn.cis.cis455.m1.handling;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.SparkController;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.interfaces.HttpResponse;
import edu.upenn.cis.cis455.m1.interfaces.Response;

/**
 * Contains methods to assist with finding and reading from files.
 * @author Eliot Herbst
 *
 */
public class FileRequestHandler {
	final static Logger logger = LogManager.getLogger(FileRequestHandler.class);
	
	/**
	 * Gets and returns {@code File} from path + directory, or if the path + directory is a directory, 
	 * an index.html file in the directory if it exists.
	 * @param path The path of the file
	 * @return File The file if it is found
	 * @throws HaltException throws 404 if the file does not exist, throws 403 if the file
	 *  is outside the allowed directory, throws 500 on IOException.
	 */
	public static File getFile(String path, String directory) throws HaltException {
		//Gets File object at the directory + path
		String completePath = directory + path;
		File file = new File(completePath);
		File dir = new File(directory);
		
		// If the file is a directory looks for /index.html
		if (file.isDirectory()) {
			completePath = completePath + File.separator + "index.html";
			file = new File(completePath);
		}
		
		// Throws 404 not found if file does not exist
		if (!file.exists()) {
			throw new HaltException(HttpServletResponse.SC_NOT_FOUND, "FILE NOT FOUND, there is no file " + path);
		}
		// Checks the canonicalPath (without . or ..) of the file
		// in order to make sure that the file is in the server's directory
		String canonicalPath;
		try {
			String directoryFull = dir.getCanonicalPath();
			canonicalPath = file.getCanonicalPath();
			// throw 403 if file is outside of directory
			if(canonicalPath.indexOf(directoryFull) != 0) {
				throw new HaltException(HttpServletResponse.SC_FORBIDDEN, "ACCESS FORBIDDEN: " + path + " is outside the server's directory");
			}
		} catch (IOException e) {
			// Throws 500 Server Error
			throw new HaltException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "INTERNAL SERVER ERROR: " + e.getMessage());
		}
		
		//Return File
		return file;
	}
	
	/**
	 * Converts from a {@code File} to an {@code HttpResponse}
	 * @param file The file to convert to
	 * @return An HttpResponse containing the File to return as a byte[]
	 * @throws HaltException
	 */
	public static HttpResponse fileToResponse(File file) throws HaltException {
		// Gets the File's path
		Path filePath = file.toPath();
		
		// Gets the ContentType of the File
		String type;
		try {
			type = Files.probeContentType(filePath);
		} catch (IOException e) {
			// Server Error
			throw new HaltException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "INTERNAL SERVER ERROR: " + e.getMessage());
		}
		// Creates a new HttpReponse Object
		HttpResponse rsp = new HttpResponse();
		
		// Reads the byte[] from the file
		byte[] fileBytes;
		try {
			fileBytes = Files.readAllBytes(filePath);
		} catch (IOException e) {
			// Server Error
			throw new HaltException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "INTERNAL SERVER ERROR: " + e.getMessage());
		}
		
		// Sets HttpResponse Values
		rsp.bodyRaw(fileBytes);
		rsp.type(type);
		rsp.status(200);
		
		// Returns HttpResponse
		return rsp;
	}
		
}
