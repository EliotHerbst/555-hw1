package edu.upenn.cis.cis455.m1.handling;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.SparkController;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.interfaces.HttpRequest;
import edu.upenn.cis.cis455.m1.interfaces.HttpResponse;
import edu.upenn.cis.cis455.m1.interfaces.Request;
import edu.upenn.cis.cis455.m1.interfaces.Response;

/**
 * Handles marshaling between HTTP Requests and Responses
 */
public class HttpIoHandler {
    final static Logger logger = LogManager.getLogger(HttpIoHandler.class);
    
    public static DateTimeFormatter rfc822 = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z", Locale.US).withZone(ZoneId.of("GMT"));
    public static DateTimeFormatter rfc850 = DateTimeFormatter.ofPattern("EEEE, dd-MMM-yy HH:mm:ss z", Locale.US).withZone(ZoneId.of("GMT"));
    public static DateTimeFormatter ansi = DateTimeFormatter.ofPattern("EEE MMM d HH:mm:ss yyyy", Locale.US).withZone(ZoneId.of("GMT"));
    
    public static String statusCodeMessage(int statusCode) {
    	switch(statusCode) {
    		case 100 : return "Continue";
    		case 200 : return "OK";
    		case 304 : return "Not Modified";
    		case 400 : return "Bad Request";
    		case 403 : return "Forbidden";
    		case 404 : return "Not Found";
    		case 405 : return "Method Not Allowed";
    		case 412 : return "Precondition Failed";
    		case 500 : return "Internal Server Error";
    		case 505 : return "HTTP Version Not Supported";
    		default : return "OK";
    	}
    }

    /**
     * Sends an exception back, in the form of an HTTP response code and message.
     * Returns true if we are supposed to keep the connection open (for persistent
     * connections).
     */
    public static boolean sendException(Socket socket, Request request, HaltException except) {
    	//logger.info("Responding with " + except.statusCode());
    	HttpResponse resp = new HttpResponse();
    	// Get Output Stream
    	try {
    		//Create Message
			String initialLine = "HTTP/1.1 " + except.statusCode() +  " " + statusCodeMessage(except.statusCode()) + "\r\n";
    		String hdrs = initialLine + resp.getHeaders() + "\r\n";
    		String message = hdrs;
			byte[] output = message.getBytes();
			OutputStream os = socket.getOutputStream();
			os.write(output);
			os.flush();
			os.close();
		} catch (IOException e) {
			logger.info(e.getLocalizedMessage());
			// Can't write to os
		}
    	
    	//Terminate Connection
    	return false;
    }

    /**
     * Sends data back. Returns true if we are supposed to keep the connection open
     * (for persistent connections).
     */
    public static boolean sendResponse(Socket socket, HttpRequest req, Response response) {
        try {
        	// Check Protocol Version
        	if (req.protocol().equals("HTTP/1.1")) {
        		// Http 1.1 must have host header
        		if (!req.headers().contains("host")) {
        			throw new HaltException(400, "BAD REQUEST: HTTP/1.1 requires host header");
        		}
        		if (req.headers().contains("expect") && req.headers("expect").equals("100-continue")) {
        			// Continue Message with empty line after
        			String cont = "HTTP/1.1 100 Continue/r/n/r/n";
        			try {
        				// Write message to socket
						OutputStream os = socket.getOutputStream();
						os.write(cont.getBytes());
						os.flush();
						os.close();
					} catch (IOException e) {
						// Server Error
						throw new HaltException(500, "INTERNAL SERVER ERROR: " + e.getMessage());
					}
        		}
        	} else if (req.protocol().equals("HTTP/1.0")) {
        		// Http 1.0
        	} else {
        		// Http Version Unimplemented
        		throw new HaltException(505, "HTTP VERSION NOT SUPPORTED, this server supports HTTP/1.1 and HTTP/1.0");
        	}
        	// Method must be GET or HEAD
    		if (!req.requestMethod().equals("GET") && !req.requestMethod().equals("HEAD")) {
    			//Unsupported Request Type
    			throw new HaltException(405, "HTTP METHOD NOT SUPPORTED, this server supports GET and HEAD");
    		}
    		
    		// Check if special method
    		if (req.pathInfo().equals("/shutdown") && req.requestMethod().equals("GET")) {
    			// Message
    			String initialLine = "HTTP/1.1 200 OK\r\n";
    			HttpResponse rs = new HttpResponse();
    			String hdrs = initialLine + rs.getHeaders() + "\r\n";
    			byte[] output = hdrs.getBytes();

    			// Write message to socket
        		OutputStream os;
				try {
					os = socket.getOutputStream();
					os.write(output);
					os.flush();
					os.close();
				} catch (IOException e) {
					throw new HaltException(500, "INTERNAL SERVER ERROR: " + e.getMessage());
				}
        		
    			// Call Spark Controller Stop
    			SparkController.stop();
    			
    		} else if (req.pathInfo().equals("/control")) {
    			List<String> workerStatus = SparkController.webService.getWorkerStatus();
    			byte[] output = HttpIoHandler.makeControl(workerStatus);
    			// Write message to socket
        		OutputStream os;
				try {
					os = socket.getOutputStream();
					os.write(output);
					os.flush();
					os.close();
				} catch (IOException e) {
					throw new HaltException(500, "INTERNAL SERVER ERROR: " + e.getMessage());
				}
    		} else {
    			// The root directory of server
        		String directory = SparkController.webService.getStaticFileLocation();
        		
        		// Response
        		File f = FileRequestHandler.getFile(req.pathInfo(), directory);
        		HttpResponse rsp = FileRequestHandler.fileToResponse(f);
        		// Gets last modified time as ZonedDateTime is GMT
        		ZonedDateTime lastModified = Instant.ofEpochMilli(f.lastModified()).atZone(ZoneId.of("GMT"));
        		String lastModifiedHeader = "Last-Modified: " + lastModified.format(rfc822).toString() + "\r\n";
        		// Check for If Modified or If Unmodified Headers
        		if (req.headers().contains("if-modified-since")) {
        			// Get the time given in header
        			String since = req.headers("if-modified-since");
        			
        			// Convert to ZonedDateTime
        			ZonedDateTime zdt;
        			try {
        				zdt = ZonedDateTime.parse(since, rfc822);
        			} catch (DateTimeParseException e) {
        				try {
        					zdt = ZonedDateTime.parse(since, rfc850);
        				} catch (DateTimeParseException el) {
        					try {
        						zdt = ZonedDateTime.parse(since, ansi);
        					} catch (DateTimeParseException eli) {
        						throw new HaltException(400, "BAD REQUEST: Date is not in a valid format");
        					}
            				
            			}
        			}
        			//logger.info(lastModified.toString() + " " + lastModified.isAfter(zdt));
        			if (lastModified.isBefore(zdt)) {
        				// Precondition Failed
        				throw new HaltException(304, "NOT MODIFIED since " + zdt.toString());
        			}
        		}
        		if (req.headers().contains("if-unmodified-since")) {
        			// Get the time given in header
        			String since = req.headers("if-unmodified-since");
        			
        			// Convert to ZonedDateTime
        			ZonedDateTime zdt;
        			try {
        				zdt = ZonedDateTime.parse(since, rfc822);
        			} catch (DateTimeParseException e) {
        				try {
        					zdt = ZonedDateTime.parse(since, rfc850);
        				} catch (DateTimeParseException el) {
        					try {
        						zdt = ZonedDateTime.parse(since, ansi);
        					} catch (DateTimeParseException eli) {
        						throw new HaltException(400, "BAD REQUEST: Date is not in a valid format");
        					}
            				
            			}
        			}
        			
        			if (lastModified.isAfter(zdt)) {
        				// Precondition Failed
        				throw new HaltException(412, "PRECONDITION FAILED, modified since" + zdt.toString());
        			}
        		}
        		try {
        			// Create message
        			String initialLine = "HTTP/1.1 200 OK\r\n";
        			String hdrs = initialLine + rsp.getHeaders() + lastModifiedHeader + "\r\n";
        			byte[] hdrsB = hdrs.getBytes();
        			byte[] output = hdrsB;
        			if (req.requestMethod().equals("GET")) {
        				output = new byte[hdrsB.length + rsp.bodyRaw().length];
            			for(int i = 0; i < output.length; i++) {
            				output[i] = i < hdrsB.length ? hdrsB[i] : rsp.bodyRaw()[i - hdrsB.length];
            			}
        			}
        			// Write message to socket
            		OutputStream os = socket.getOutputStream();
            		os.write(output);
					os.flush();
					os.close();
					
				} catch (IOException e) {
					// Server Error
					throw new HaltException(500, "INTERNAL SERVER ERROR: " + e.getMessage());
				}
        		
        		//logger.info("Responded with 200 OK");
        		
        		// Close connection
        		return false;
    		}
    		
        } catch (HaltException e) {
        	//logger.info(e.statusCode());
        	HttpIoHandler.sendException(socket, null, e);
        } 
        return false;
    }
    
    /**
     * Reads message from socket into a String
     * @param socket socket to read from
     * @return String message on Socket
     * @throws HaltException
     */
    public static BufferedReader readFromSocket(Socket socket) throws HaltException {
    	try {
    		// Gets BufferedReader from InputStream
    		InputStream in = socket.getInputStream();
    		InputStreamReader reader = new InputStreamReader(in);
    		BufferedReader br = new BufferedReader(reader);
    		
    		return br;
    	} catch(IOException e) {
    		//Server Error
    		throw new HaltException(500, "INTERNAL SERVER ERROR: "  + e.getMessage());
    	}
    }
    
    /**
     * Creates the control page
     * @param workers {@code List<String>} containing information about the workers
     * @return
     */
    public static byte[] makeControl (List<String> workers) {
    	// Initial Line of the response
    	String initialLine = "HTTP/1.1 200 OK\r\n";
    	// Get Standard response headers (Date, etc)
    	HttpResponse resp = new HttpResponse();
    	String headers = resp.getHeaders();
    	// Raw HTML as String
    	String contentType = "content-type: text/html\r\n";
    	String page = "<!DOCTYPE html>\r\n" +
    				  "<html>\r\n" +
    				  "<head>\r\n" +
    				  "    <title>Control Panel</title>\r\n" +
    				  "<body>\r\n" +
    				  "<h1>Control Panel</h1>\r\n";
    	// Line for each worker
    	String middle = "";
    	for(String s : workers) {
    		middle += "<p>" + s + "</p>";
    	}
    	String pEnd = "<button onclick=\"window.location.href=\'/shutdown\'\">Shutdown Server</button>\r\n" + 
    				  "</body>\r\n" + 
    				  "</html>";
    	// Combine and return		  
    	String body =  page + middle + pEnd;
    	String contentLength = "content-length: " + body.length();
    	String html = initialLine + headers + contentType + contentLength + "\r\n\r\n" + body;
    	return html.getBytes();
    }
}
