package edu.upenn.cis.cis455.m1.handling;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.interfaces.HttpRequest;
import edu.upenn.cis.cis455.m1.interfaces.Request;

public class HttpParser {
	final static Logger logger = LogManager.getLogger(HttpParser.class);
	
	public static HttpRequest decodeRequest(BufferedReader br) throws HaltException {
		// Http Request to be filled and returned
		HttpRequest req = new HttpRequest();
		
		// First line of request
		String requestLine;
		try {
			requestLine = br.readLine();
		} catch (IOException e) {
			// Server Error
			throw new HaltException(500, "INTERNAL SERVER ERROR: " + e.getMessage());
		}
		
		// Throw 400 Bad Request if no requestLine, means the Request was empty
		if (requestLine == null) {
			throw new HaltException(400, "BAD REQUEST: no requestLine, Request was Empty");
		}
		
		// Divide requestLine into tokens
		StringTokenizer st = new StringTokenizer(requestLine);
		// Throw 400 Bad Request if there are no tokens, means request had no meaningful characters
		if(!st.hasMoreTokens()) {
			throw new HaltException(400, "BAD REQUEST: missing HTTP method");
		}
		// First Token is Http method (GET, POST, Etc.)
		String HttpMethod = st.nextToken();
		
		// Throw 400 Bad Request if there are no more tokens, means request was just an Http Method
		if(!st.hasMoreTokens()) {
			throw new HaltException(400, "BAD REQUEST: missing url");
		}
		
		// Next token is url
		String url = st.nextToken();
		
		// Throw 400 Bad Request if there is no Http Version
		if(!st.hasMoreTokens()) {
			throw new HaltException(400, "BAD REQUEST: missing HTTP Version");
		}
		
		// Next token is protocolVersion
		String protocolVersion = st.nextToken();
		
		// Map to hold headers in
		Map<String, String> headers = new HashMap<String, String>();
		
		
		try {
			// Read next Line
			String line = br.readLine();
			// Keep Track of previous key if line wraps
			String lastKey = null;
			// Loops while the line isn't null or only containing blank characters (maybe use isBlank() here?)
			while(line != null && !line.trim().isEmpty()) {
				// Get index of :
				int p = line.indexOf(":");
				
				// Previous Line hasn't wrapped
				if (p >= 0) {
					// Get the key before :
					String key = line.substring(0, p).trim().toLowerCase(Locale.US);
					// Add header key value pair
					headers.put(key, line.substring(p + 1).trim());
					// For Wrap Situation
					lastKey = key;
				} else if (lastKey != null && line.startsWith(" ") || line.startsWith("\t")) {
					// the wrapped part
					String newPartValue = line.trim();
					// Add to the previous Key
					headers.put(lastKey,  headers.get(lastKey) + newPartValue);
					
				}
				
				line = br.readLine();
				
			}
		} catch (IOException e) {
			// Server Error
			throw new HaltException(500, "INTERNAL SERVER ERROR: " + e.getMessage());
		}
		
		// Settings request values
		req.method(HttpMethod);
		
		req.protocol(protocolVersion);
		
		// Add these headers if they exist
		
		if (headers.containsKey("user-agent")) {
			req.userAgent(headers.get("user-agent"));
		}
		
		if (headers.containsKey("accept")) {
			req.accept(headers.get("accept"));
		}
		if (headers.containsKey("connection")) {
			req.persistentConnection(headers.get("connection").equals("keep-alive") || headers.get("connection").equals("Keep-Alive"));
		}
		
		if (headers.containsKey("host")) {
			req.host(headers.get("host"));
		}
		
		if (headers.containsKey("if-modified-since")) {
			req.ifModifiedSince(headers.get("if-modified-since"));
		}
		
		if (headers.containsKey("if-unmodified-since")) {
			req.ifUnmodifiedSince(headers.get("if-unmodified-since"));
		}
		
		req.url(url);
		
		// Set Uri to be url up to ? if it exists
		int index = url.indexOf("?");
		if (index != -1) {
			req.uri(url.substring(0, index));
		} else {
			req.uri(url);
		}
		
		// Set Path to be uri after host if it exists
		if (req.uri().contains("http")) {
			String afterHttp = req.uri().substring(7); // h t t p : / /
			int slashIndex = afterHttp.indexOf("/");
			if (slashIndex != -1) {
				req.pathInfo(afterHttp.substring(slashIndex));
			} else {
				// The path is just /
				req.pathInfo("/");
			}
			
		} else {
			// just path
			req.pathInfo(req.uri());
		}
		/*
		// If Request was chunk encoded then there could be footers
		if (req.headers().contains("transfer-encoding")) {
			if (req.headers("transfer-encoding").equals("chunked")) {
				try {
					HttpParser.decodeChunked(req, br);
				} catch (IOException e) {
					// Server Error
					throw new HaltException(500);
				}
			}
		}
		*/
		return req;	
	}

	/*
	public static void decodeChunked(HttpRequest req, BufferedReader br) throws IOException {
		String line = br.readLine();
		// Read lines up until the 0 line
		while(line != null && line.equals("0")) {
			line = br.readLine();
		}
		// Get the next line, footers start here
		line = br.readLine();
		
		// Map to hold footers in
		Map<String, String> footers = new HashMap<String, String>();
		
		// Keep Track of previous key if line wraps
		String lastKey = null;
		// Loops while the line isn't null or only containing blank characters (maybe use isBlank() here?)
		while(line != null && !line.trim().isEmpty()) {
			// Get index of :
			int p = line.indexOf(":");
			
			// Previous Line hasn't wrapped
			if (p >= 0) {
				// Get the key before :
				String key = line.substring(0, p).trim().toLowerCase(Locale.US);
				// Add footer key value pair
				footers.put(key, line.substring(p + 1).trim());
				// For Wrap Situation
				lastKey = key;
			} else if (lastKey != null && line.startsWith(" ") || line.startsWith("\t")) {
				// the wrapped part
				String newPartValue = line.trim();
				// Add to the previous Key
				footers.put(lastKey,  footers.get(lastKey) + newPartValue);
			}
			line = br.readLine();
		}
		
		// Merge header set 
		Set<String> headers = req.headers();
		headers.addAll(footers.keySet());
		req.setHeaderSet(headers);
		
		// Merge header map
		Map<String, String> headerMap = req.headerMap();
		for(String key : footers.keySet()) {
			headerMap.put(key, footers.get(key));
		}
		req.setHeaders(headerMap); 
	}
	*/
}
