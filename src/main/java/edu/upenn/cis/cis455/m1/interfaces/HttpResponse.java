package edu.upenn.cis.cis455.m1.interfaces;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Implementatin of Response Class
 * @author vagrant
 *
 */
public class HttpResponse extends Response {
	
	private Map<String, String> headers; // Map to store headers
    public static DateTimeFormatter rfc822 = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z", Locale.US).withZone(ZoneId.of("GMT"));

	
	/**
	 * Create a new Http Reponse Instance
	 */
	public HttpResponse() {
		// Initialize HashMap for headers
		this.headers = new HashMap<String, String>();
		this.headers.put("Server", "EliotCis455"); // Add Server Header
		// Get Current Time and add to Date
		ZonedDateTime zdt = ZonedDateTime.ofInstant(Instant.now(), ZoneId.of("GMT"));
		String formattedDate = rfc822.format(zdt);
		this.headers.put("Date", formattedDate);
		this.headers.put("Connection", "close"); // Add Connection Close (This server does not support persistent connections
	}
	
	/**
	 * Updates content Type and adds content-type header
	 */
	@Override
	public void type(String contentType) {
		this.contentType = contentType;
		this.headers.put("content-type", contentType);
	}
	
	/**
	 * Updates body and adds content-length header
	 */
	@Override
	public void bodyRaw(byte[] body) {
		this.body = body;
		this.headers.put("content-length", "" + this.body.length);
	}
	
	/**
	 * Updates body and adds content-length header
	 */
	@Override
	public void body(String body) {
		this.body = body == null ? null : body.getBytes();
		this.headers.put("content-length", "" + this.body.length);
	}

	
	/**
	 *  Returns Headers as a String
	 */
	@Override
	public String getHeaders() {
		String hdrs = ""; // String to store headers in
		// Adds headers to hdrs String line by line
		for (String s : this.headers.keySet()) {
			hdrs += s + ": " + this.headers.get(s) + "\r\n";
		}
		return hdrs;
	}
}
