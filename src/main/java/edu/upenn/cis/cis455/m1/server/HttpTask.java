package edu.upenn.cis.cis455.m1.server;

import java.net.Socket;

public class HttpTask {
    Socket requestSocket;

    /**
     * Initializes the {@code HttpTask} with a {@code Socket}
     * @param socket The incoming {@code Socket}
     */
    public HttpTask(Socket socket) {
        requestSocket = socket;
    }
    
    /**
     * Returns the Incoming socket 
     * @return Socket The {@code Socket} to respond to
     */
    public Socket getSocket() {
        return requestSocket;
    }
}
