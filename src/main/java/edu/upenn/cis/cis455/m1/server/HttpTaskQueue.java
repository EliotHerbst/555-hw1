package edu.upenn.cis.cis455.m1.server;

import java.util.LinkedList;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Stub class for implementing the queue of HttpTasks
 */
public class HttpTaskQueue {

		final static Logger logger = LogManager.getLogger(HttpTaskQueue.class);
		
		private Queue<HttpTask> tasks;
		private int maximumSize;
		private volatile boolean stop = false;
		
		/**
		 * Creates a {@code HttpTaskQueue} object with a maximum queue size
		 * @param maximumSize The {@code int} maximum size of the queue
		 */
		public HttpTaskQueue(int maximumSize) {
			this.tasks = new LinkedList<HttpTask>();
			this.maximumSize = maximumSize;
		}
		
		public void stop() {
			if (this.stop != true) {
				this.stop = true;
			}
		}
		
		/**
		 * Adds a {@code HttpTask} to the {@code HttpTaskQueue}. Waits while the Queue is of {@code maximumSize}
		 * 
		 * @param newTask The new {@code HttpTask} to add
		 * @return boolean {@code True} if the add succeeded, otherwise {@code False}
		 */
		public synchronized boolean addTask(HttpTask newTask) throws InterruptedException {
			//logger.info("Starting Adding Task");
			while(!stop && this.tasks.size() >= this.maximumSize) {
				//logger.info("HttpTaskQueue is full, waiting: size = " + this.tasks.size());
				wait(1);
			}
			//logger.info("Adding Task");
			boolean result = this.tasks.offer(newTask);
			notify();
			return result;
		}
		
		/**
		 * Returns the next {@code HttpTask} in the {@code HttpTaskQueue}.
		 * 
		 * @return HttpTask The next HttpTask or {@code null} if the TaskQueue is Empty
		 * @throws InterruptedException 
		 */
		public synchronized HttpTask nextTask() throws InterruptedException {
			while(!stop && this.tasks.isEmpty()) {
				wait(5);
			}
			HttpTask nextTask = this.tasks.poll();
			notify();
			return nextTask;
			
		}
}
